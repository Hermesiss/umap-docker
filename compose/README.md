# Example compose

## Usage

1. Copy `.env.example` to `.env` and edit the values.
1. If using [https://gitlab.com/wp-id/docker/traefik-proxy/](traefik proxy), copy `docker-compose.override-proxy.yml` to `docker-compose.override.yml` and add some more labels for https, etc. Otherwise, create an override file and expose port `8000` of the `app` service.
1. Customise env variables as needed.
1. Run the containers: `docker-compose up -d`
1. When all containers are up, create superadmin user:
    ```
    docker-compose exec app /bin/bash
    umap createsuperuser
    exit
    ```
1. To provide custom theme, mount your theme to `/srv/umap/custom`. See [documentation](https://umap-project.readthedocs.io/en/latest/custom/).
