#!/bin/sh

# Variants.
LATEST_IMAGE_NAME="${IMAGE_NAME}:latest"
NEW_IMAGE_TAG="${IMAGE_NAME}:${CI_COMMIT_TAG}"

echo "$DOCKER_PASSWORD" | docker login -u "$DOCKER_USER" --password-stdin $DOCKER_REGISTRY

docker pull $LATEST_IMAGE_NAME
docker tag $LATEST_IMAGE_NAME $NEW_IMAGE_TAG
docker push $NEW_IMAGE_TAG
