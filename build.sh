#!/bin/sh

set -e

if [ $CI_COMMIT_REF_NAME = "main" ]; then
    # Docker hub
    REGISTRY="${DOCKER_REGISTRY}"
    REGISTRY_PASS="${DOCKER_PASSWORD}"
    REGISTRY_USER="${DOCKER_USER}"
    TAG=latest
else
    # GitLab docker registry
    REGISTRY="${CI_REGISTRY}"
    REGISTRY_PASS="${CI_REGISTRY_PASSWORD}"
    REGISTRY_USER="${CI_REGISTRY_USER}"
    TAG="${CI_COMMIT_SHA}"
    # Override IMAGE_NAME set in repo settings.
    IMAGE_NAME="${CI_REGISTRY_IMAGE}"
fi

PREV_IMAGE_NAME="${IMAGE_NAME}:latest"
BUILD_IMAGE_NAME="${IMAGE_NAME}:${TAG}"
BUILD_ARGS=""

echo "$REGISTRY_PASS" | docker login -u "$REGISTRY_USER" --password-stdin $REGISTRY

docker pull $PREV_IMAGE_NAME || true
eval "docker build --cache-from ${PREV_IMAGE_NAME} -t ${BUILD_IMAGE_NAME} ${BUILD_ARGS} ."
docker push $BUILD_IMAGE_NAME
