FROM node:14 AS builder

ARG UMAP_VERSION=1.2.3

RUN mkdir -p /tmp/umap \
    && curl -sSL https://github.com/umap-project/umap/archive/${UMAP_VERSION}.tar.gz | tar -C /tmp/umap -xz --strip-components=1 \
    && cd /tmp/umap \
    && npm install --production \
    && make vendors \
    && rm -rf node_modules

FROM python:3.8-slim

ARG TINI_VERSION=v0.14.0

ENV PYTHONUNBUFFERED=1 \
    UMAP_SETTINGS=/srv/umap/settings.py \
    PORT=8000

RUN mkdir -p /srv/umap/custom \
    && mkdir /srv/umap/custom/static \
    && mkdir /srv/umap/custom/templates \
    && mkdir /srv/umap/data \
    && mkdir /srv/umap/uploads \
    && mkdir /usr/share/man/man1 \
    && useradd umap -d /srv/umap

COPY --from=builder /tmp/umap /srv/umap

COPY . /srv/umap/

WORKDIR /srv/umap

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        binutils \
        build-essential \
        curl \
        gdal-bin \
        gettext \
        git \
        libffi-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        liblcms2-dev \
        libpq-dev \
        libproj-dev \
        libtiff5-dev \
        libwebp-dev \
        mime-support \
        sqlite3 \
        uwsgi \
        zlib1g-dev \
    && pip install --no-cache -r requirements-docker.txt && pip install . \
    && apt-get remove -y \
        binutils \
        libffi-dev \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        liblcms2-dev \
        libproj-dev \
        libtiff5-dev \
        libwebp-dev \
        zlib1g-dev \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Add Tini
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini

VOLUME [ "/srv/umap/custom", "/srv/umap/data", "/srv/umap/uploads" ]

EXPOSE 8000

ENTRYPOINT ["/tini", "--"]

CMD ["/srv/umap/docker-entrypoint.sh"]
